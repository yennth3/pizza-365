//import VoucherModel vào file
const { default: mongoose } = require('mongoose');
const voucherModel = require('../models/voucherModel');



//get All Vouchers
const getAllVouchers = (request, response) => {
    //b1: thu thập dữ liệu(bỏ qua)
    //b2: kiểm tra dữ liệu (bỏ qua)
    //b3: thực hiện thao tác dữ liệu
    voucherModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        }
        else {
            response.status(200).json({
                data
            })
        }
    })
};

//getVoucherById
const getVoucherById = (request, response) => {
    //b1: thu thập dữ liệu

    let id = request.params.voucherid;
    //b2: kiểm tra dữ liệu 
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "id is invalid!"
        })
    } else {
        //b3: thực hiện thao tác dữ liệu
        voucherModel.findById(id, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error : ${error.message}`
                })
            }
            else {
                response.status(200).json({
                    data
                })
            }
        })
    }
};
//create a Voucher
const createVoucher = (request, response) => {
    //b1: thu thập dữ liệu
    let body = request.body;
    console.log(body);
    //b2: kiểm tra dữ liệu
    if (!body.maVoucher) {
        response.status(400).json({
            message: "maVoucher is require"
        })
    } else if (!body.phanTramGiamGia) {
        response.status(400).json({
            message: "phanTramGiamGia is require"
        })
    } else {
        //b3: thực hiện
        let voucher = {
            _id: mongoose.Types.ObjectId(),
            maVoucher: body.maVoucher,
            phanTramGiamGia: body.phanTramGiamGia,
            ghiChu: body.ghiChu
        }
        voucherModel.create(voucher, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error : ${error.message}`
                })
            } else {
                response.status(201).json({
                    data
                })
            }
        });
    }

};

////update Voucher
const updateVoucher = (request, response) => {
    //b1: thu thập dữ liệu
    let id = request.params.voucherid;
    let body = request.body;
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "id is invalid!"
        })
    }   if (!body.maVoucher) {
        response.status(400).json({
            message: "maVoucher is require"
        })
    } else if (!body.phanTramGiamGia) {
        response.status(400).json({
            message: "phanTramGiamGia is require"
        })
    }
    else {
        //b3: thực hiện
        let voucher = {
            maVoucher: body.maVoucher,
            phanTramGiamGia: body.phanTramGiamGia,
            ghiChu: body.ghiChu
        }
        voucherModel.findByIdAndUpdate(id, voucher, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error : ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        });
    }
};

//delete Voucher
const deleteVoucher = (request, response) => {
    //b1: thu thập
    let id = request.params.voucherid;
    //b2: kiểm tra
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "id is invalid!"
        })
    } else {
        //b3:
        voucherModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error : ${error.message}`
                })
            } else {
                response.status(204).json({
                    data
                })
            }
        });
    }
}

//get All Vouchers
const getVoucherByVoucherCode = (request, response) => {
    //b1: thu thập dữ liệu(bỏ qua)
    let voucherCode = request.params.voucherId;
    let condition={};
    if(voucherCode){
        condition.maVoucher=voucherCode;
    }
    //b2: kiểm tra dữ liệu (bỏ qua)
    //b3: thực hiện thao tác dữ liệu
    voucherModel.findOne(condition,(error, data) => {
        if (error) {
            response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        }
        else {
            response.status(200).json({
                data
            })
        }
    })
};

module.exports = { getAllVouchers, createVoucher, updateVoucher, deleteVoucher, getVoucherById, getVoucherByVoucherCode }
