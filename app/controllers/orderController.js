const { default: mongoose } = require('mongoose');

const orderModel = require('../models/orderModel');
const userModel = require('../models/userModel');
const voucherModel = require('../models/voucherModel');
const drinkModel = require('../models/drinkModel');

const createOrderOfUser = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userid;
    let requestBody = request.body;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User Id is invalid"
        })
    }
    if (!requestBody.orderCode) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "orderCode is invalid"
        })
    }
    if (!requestBody.pizzaSize) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "pizzaSize is invalid"
        })
    }
    if (!requestBody.pizzaType) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "pizzaType is invalid"
        })
    }
    if (!requestBody.status) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "status is invalid"
        })
    }


    //B3: Thao tác với cơ sở dữ liệu
    let newOrderInput = {
        _id: mongoose.Types.ObjectId(),
        orderCode: requestBody.orderCode,
        pizzaSize: requestBody.pizzaSize,
        pizzaType: requestBody.pizzaType,
        status: requestBody.status
    }

    orderModel.create(newOrderInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            userModel.findByIdAndUpdate(userId,
                {
                    $push: { orders: data._id }
                },
                (err, updatedUser) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return response.status(201).json({
                            status: "Create Order Success",
                            data: data
                        })
                    }
                }
            )
        }
    })
}

const getAllOrderOfUser = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userid;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User Id is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    userModel.findById(userId)
        .populate("orders")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get data success",
                    data: data.orders
                })
            }
        })
}

const getOrderById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = request.params.orderId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "orderId is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get order success",
                data: data
            })
        }
    })
}

const updateOrderById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = request.params.orderId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "orderId is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let orderUpdate = {
        orderCode: bodyRequest.orderCode,
        pizzaSize: bodyRequest.pizzaSize,
        pizzaType: bodyRequest.pizzaType,
        status: bodyRequest.status
    }

    orderModel.findByIdAndUpdate(orderId, orderUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update success",
                data: data
            })
        }
    })
}

const deleteOrderById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userid;
    let orderId = request.params.orderId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "userId is not valid"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "orderId is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.findByIdAndDelete(orderId, (error) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            // Sau khi xóa xong 1 order khỏi collection cần xóa thêm orderId trong user đang chứa nó
            userModel.findByIdAndUpdate(userId,
                {
                    $pull: { orders: orderId }
                },
                (err, updatedCourse) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return response.status(204).json({
                            status: "Success: Delete success"
                        })
                    }
                })
        }
    })
}

const createNewOrder = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let requestBody = request.body;
    let userEmail = requestBody.email;
    let orderCode = Math.random().toString(36).substring(2, 7);
    let userId;
    let voucherCode = requestBody.idVoucher;
    let drinkCode = requestBody.idLoaiNuocUong;

    //B2: Validate dữ liệu
    if (!requestBody.kichCo) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "kichCo is invalid"
        })
    }
    if (!requestBody.duongKinh) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "duongKinh is invalid"
        })
    }
    if (!requestBody.suon) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "suon is invalid"
        })
    }
    if (!requestBody.salad) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "salad is invalid"
        })
    }
    if (!requestBody.loaiPizza) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "loaiPizza is invalid"
        })
    }
    if (!requestBody.hoTen) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "hoTen is invalid"
        })
    }
    if (!requestBody.email) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "email is invalid"
        })
    }
    if (!requestBody.diaChi) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "diaChi is invalid"
        })
    }
    if (!requestBody.soDienThoai) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "soDienThoai is invalid"
        })
    }
    // if (requestBody.idVoucher && !mongoose.Types.ObjectId.isValid(requestBody.idVoucher)) {
    //     return response.status(400).json({
    //         status: "Error 400: Bad Request",
    //         message: "idVoucher is invalid"
    //     })
    // }
    // if (!mongoose.Types.ObjectId.isValid(requestBody.idLoaiNuocUong)) {
    //     return response.status(400).json({
    //         status: "Error 400: Bad Request",
    //         message: "idLoaiNuocUong is invalid"
    //     })
    // }
    drinkModel.findOne({
        maNuocUong: drinkCode
    }, (err, drinkExist) => {
        if (!drinkExist) {
            return response.status(400).json({
                status: "Error 400: Bad Request",
                message: "drinkCode is invalid"
            })
        } else {
            voucherModel.findOne({
                maVoucher: voucherCode
            }, (err, voucherExist) => {
                if (voucherExist) {
                    let newOrderInput = {
                        _id: mongoose.Types.ObjectId(),
                        orderId: orderCode,
                        kichCo: requestBody.kichCo,
                        duongKinh: requestBody.duongKinh,
                        suon: requestBody.suon,
                        salad: requestBody.salad,
                        loaiPizza: requestBody.loaiPizza,
                        idVoucher: voucherExist._id,
                        thanhTien: requestBody.thanhTien,
                        giamGia: requestBody.giamGia,
                        idLoaiNuocUong: drinkExist._id,
                        soLuongNuoc: requestBody.soLuongNuoc,
                        hoTen: requestBody.hoTen,
                        email: requestBody.email,
                        soDienThoai: requestBody.soDienThoai,
                        diaChi: requestBody.diaChi,
                        loiNhan: requestBody.loiNhan,
                        trangThai: "open"
                    }

                    //B3: Thao tác với cơ sở dữ liệu
                    userModel.findOne({
                        email: userEmail
                    }, (error, userExist) => {
                        if (error) {
                            return response.status(500).json({
                                status: "Error 500: Internal server error",
                                message: errorFindUser.message
                            })
                        } else {
                            //nếu user chưa tồn tại=>tạo user mới
                            if (!userExist) {
                                let newInput = {
                                    _id: mongoose.Types.ObjectId(),
                                    fullName: requestBody.hoTen,
                                    email: requestBody.email,
                                    address: requestBody.diaChi,
                                    phone: requestBody.soDienThoai
                                }

                                userModel.create(newInput, (error, userCreated) => {
                                    if (error) {
                                        return response.status(500).json({
                                            status: "Error 500: Internal server error",
                                            message: error.message
                                        })
                                    } else {
                                        userId = userCreated._id;
                                        orderModel.create(newOrderInput, (error, data) => {
                                            if (error) {
                                                return response.status(500).json({
                                                    status: "Error 500: Internal server error",
                                                    message: error.message
                                                })
                                            } else {
                                                userModel.findByIdAndUpdate(userId,
                                                    {
                                                        $push: { orders: data._id }
                                                    },
                                                    (err, updatedUser) => {
                                                        if (err) {
                                                            return response.status(500).json({
                                                                status: "Error 500: Internal server error",
                                                                message: err.message
                                                            })
                                                        } else {
                                                            return response.status(201).json({
                                                                status: "Create Order Success",
                                                                data: data
                                                            })
                                                        }
                                                    }
                                                )
                                            }
                                        })
                                    }
                                })
                            } else {
                                userId = userExist._id;
                                orderModel.create(newOrderInput, (error, data) => {
                                    if (error) {
                                        return response.status(500).json({
                                            status: "Error 500: Internal server error",
                                            message: error.message
                                        })
                                    } else {
                                        userModel.findByIdAndUpdate(userId,
                                            {
                                                $push: { orders: data._id }
                                            },
                                            (err, updatedUser) => {
                                                if (err) {
                                                    return response.status(500).json({
                                                        status: "Error 500: Internal server error",
                                                        message: err.message
                                                    })
                                                } else {
                                                    return response.status(201).json({
                                                        status: "Create Order Success",
                                                        data: data
                                                    })
                                                }
                                            }
                                        )
                                    }
                                })
                            }
                        }
                    })


                } else {
                    //trường hợp không có voucher
                    let newOrderInput = {
                        _id: mongoose.Types.ObjectId(),
                        orderId: orderCode,
                        kichCo: requestBody.kichCo,
                        duongKinh: requestBody.duongKinh,
                        suon: requestBody.suon,
                        salad: requestBody.salad,
                        loaiPizza: requestBody.loaiPizza,
                        thanhTien: requestBody.thanhTien,
                        giamGia: requestBody.giamGia,
                        idLoaiNuocUong:drinkExist._id ,
                        soLuongNuoc: requestBody.soLuongNuoc,
                        hoTen: requestBody.hoTen,
                        email: requestBody.email,
                        soDienThoai: requestBody.soDienThoai,
                        diaChi: requestBody.diaChi,
                        loiNhan: requestBody.loiNhan,
                        trangThai: "open"
                    }

                    //B3: Thao tác với cơ sở dữ liệu
                    userModel.findOne({
                        email: userEmail
                    }, (error, userExist) => {
                        if (error) {
                            return response.status(500).json({
                                status: "Error 500: Internal server error",
                                message: errorFindUser.message
                            })
                        } else {
                            //nếu user chưa tồn tại=>tạo user mới
                            if (!userExist) {
                                let newInput = {
                                    _id: mongoose.Types.ObjectId(),
                                    fullName: requestBody.hoTen,
                                    email: requestBody.email,
                                    address: requestBody.diaChi,
                                    phone: requestBody.soDienThoai
                                }

                                userModel.create(newInput, (error, userCreated) => {
                                    if (error) {
                                        return response.status(500).json({
                                            status: "Error 500: Internal server error",
                                            message: error.message
                                        })
                                    } else {
                                        userId = userCreated._id;
                                        orderModel.create(newOrderInput, (error, data) => {
                                            if (error) {
                                                return response.status(500).json({
                                                    status: "Error 500: Internal server error",
                                                    message: error.message
                                                })
                                            } else {
                                                userModel.findByIdAndUpdate(userId,
                                                    {
                                                        $push: { orders: data._id }
                                                    },
                                                    (err, updatedUser) => {
                                                        if (err) {
                                                            return response.status(500).json({
                                                                status: "Error 500: Internal server error",
                                                                message: err.message
                                                            })
                                                        } else {
                                                            return response.status(201).json({
                                                                status: "Create Order Success",
                                                                data: data
                                                            })
                                                        }
                                                    }
                                                )
                                            }
                                        })
                                    }
                                })
                            } else {
                                userId = userExist._id;
                                orderModel.create(newOrderInput, (error, data) => {
                                    if (error) {
                                        return response.status(500).json({
                                            status: "Error 500: Internal server error",
                                            message: error.message
                                        })
                                    } else {
                                        userModel.findByIdAndUpdate(userId,
                                            {
                                                $push: { orders: data._id }
                                            },
                                            (err, updatedUser) => {
                                                if (err) {
                                                    return response.status(500).json({
                                                        status: "Error 500: Internal server error",
                                                        message: err.message
                                                    })
                                                } else {
                                                    return response.status(201).json({
                                                        status: "Create Order Success",
                                                        data: data
                                                    })
                                                }
                                            }
                                        )
                                    }
                                })
                            }
                        }
                    })
                }
            })
        }
    })





    // let newOrderInput = {
    //     _id: mongoose.Types.ObjectId(),
    //     orderId: orderCode,
    //     kichCo: requestBody.kichCo,
    //     duongKinh: requestBody.duongKinh,
    //     suon: requestBody.suon,
    //     salad: requestBody.salad,
    //     loaiPizza: requestBody.loaiPizza,
    //     idVoucher: requestBody.idVoucher,
    //     thanhTien:requestBody.thanhTien,
    //     giamGia:requestBody.giamGia,
    //     idLoaiNuocUong: requestBody.idLoaiNuocUong,
    //     soLuongNuoc:requestBody.soLuongNuoc,
    //     hoTen: requestBody.hoTen,
    //     email: requestBody.email,
    //     soDienThoai: requestBody.soDienThoai,
    //     diaChi: requestBody.diaChi,
    //     loiNhan: requestBody.loiNhan,
    //     trangThai: "open"
    // }

    // //B3: Thao tác với cơ sở dữ liệu
    // userModel.findOne({
    //     email: userEmail
    // }, (error, userExist) => {
    //     if (error) {
    //         return response.status(500).json({
    //             status: "Error 500: Internal server error",
    //             message: errorFindUser.message
    //         })
    //     } else {
    //         //nếu user chưa tồn tại=>tạo user mới
    //         if (!userExist) {
    //             let newInput = {
    //                 _id: mongoose.Types.ObjectId(),
    //                 fullName: requestBody.hoTen,
    //                 email: requestBody.email,
    //                 address: requestBody.diaChi,
    //                 phone: requestBody.soDienThoai
    //             }

    //             userModel.create(newInput, (error, userCreated) => {
    //                 if (error) {
    //                     return response.status(500).json({
    //                         status: "Error 500: Internal server error",
    //                         message: error.message
    //                     })
    //                 } else {
    //                     userId = userCreated._id;
    //                     orderModel.create(newOrderInput, (error, data) => {
    //                         if (error) {
    //                             return response.status(500).json({
    //                                 status: "Error 500: Internal server error",
    //                                 message: error.message
    //                             })
    //                         } else {
    //                             userModel.findByIdAndUpdate(userId,
    //                                 {
    //                                     $push: { orders: data._id }
    //                                 },
    //                                 (err, updatedUser) => {
    //                                     if (err) {
    //                                         return response.status(500).json({
    //                                             status: "Error 500: Internal server error",
    //                                             message: err.message
    //                                         })
    //                                     } else {
    //                                         return response.status(201).json({
    //                                             status: "Create Order Success",
    //                                             data: data
    //                                         })
    //                                     }
    //                                 }
    //                             )
    //                         }
    //                     })
    //                 }
    //             })
    //         } else {
    //             userId = userExist._id;
    //             orderModel.create(newOrderInput, (error, data) => {
    //                 if (error) {
    //                     return response.status(500).json({
    //                         status: "Error 500: Internal server error",
    //                         message: error.message
    //                     })
    //                 } else {
    //                     userModel.findByIdAndUpdate(userId,
    //                         {
    //                             $push: { orders: data._id }
    //                         },
    //                         (err, updatedUser) => {
    //                             if (err) {
    //                                 return response.status(500).json({
    //                                     status: "Error 500: Internal server error",
    //                                     message: err.message
    //                                 })
    //                             } else {
    //                                 return response.status(201).json({
    //                                     status: "Create Order Success",
    //                                     data: data
    //                                 })
    //                             }
    //                         }
    //                     )
    //                 }
    //             })
    //         }
    //     }
    // })


}
module.exports = {
    createOrderOfUser: createOrderOfUser,
    getAllOrderOfUser: getAllOrderOfUser,
    getOrderById: getOrderById,
    updateOrderById: updateOrderById,
    deleteOrderById: deleteOrderById,
    createNewOrder: createNewOrder
}