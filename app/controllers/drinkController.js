//import drinkModel vào file
const { default: mongoose } = require('mongoose');
const drinkModel = require('../models/drinkModel');

//get All Drinks
const getAllDrinks =  (request, response)=>{
    //b1: thu thập dữ liệu(bỏ qua)
    //b2: kiểm tra dữ liệu (bỏ qua)
    //b3: thực hiện thao tác dữ liệu
    drinkModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                message: `Internal server error : ${error.message}`
            })
        }
        else {
            response.status(200).json({
                data
            })
        }
    })
};
//getDrinkById
const getDrinkById = (request, response) => {
    //b1: thu thập dữ liệu

    let id = request.params.drinkid;
    //b2: kiểm tra dữ liệu 
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "id is invalid!"
        })
    } else {
        //b3: thực hiện thao tác dữ liệu
        drinkModel.findById(id, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error : ${error.message}`
                })
            }
            else {
                response.status(200).json({
                    data
                })
            }
        })
    }
};
//create a drink
const createDrink = (request, response)=>{
     //b1: thu thập dữ liệu
     let body = request.body;
     console.log(body);
     //b2: kiểm tra dữ liệu
     if (!body.maNuocUong) {
         response.status(400).json({
             message: "maNuocUong is require"
         })
     }else if (!body.tenNuocUong) {
        response.status(400).json({
            message: "tenNuocUong is require"
        })
    } else if (!body.donGia) {
        response.status(400).json({
            message: "donGia is require"
        })
    } else {
         //b3: thực hiện
         let drink = {
             _id: mongoose.Types.ObjectId(),
             maNuocUong: body.maNuocUong,
             tenNuocUong: body.tenNuocUong,
             donGia: body.donGia
         }
         drinkModel.create(drink, (error, data) => {
             if (error) {
                 response.status(500).json({
                     message: `Internal server error : ${error.message}`
                 })
             } else {
                 response.status(201).json({
                     data
                 })
             }
         });
     }
 
};

////update drink
const updateDrink =  (request, response)=>{
    //b1: thu thập dữ liệu
    let id = request.params.drinkid;
    let body = request.body;
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "id is invalid!"
        })
    }else if (!body.maNuocUong) {
        response.status(400).json({
            message: "maNuocUong is require"
        })
    }else if (!body.tenNuocUong) {
       response.status(400).json({
           message: "tenNuocUong is require"
       })
   } else if (!body.donGia) {
       response.status(400).json({
           message: "donGia is require"
       })
   }
     else {
        //b3: thực hiện
        let drink = {
            maNuocUong: body.maNuocUong,
            tenNuocUong: body.tenNuocUong,
            donGia: body.donGia
        }
        drinkModel.findByIdAndUpdate(id, drink, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error : ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        });
    }
};

//delete drink
const deleteDrink = (request, response)=>{
    //b1: thu thập
    let id = request.params.drinkid;
    //b2: kiểm tra
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "id is invalid!"
        })
    }else{
        //b3:
        drinkModel.findByIdAndDelete(id,(error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error : ${error.message}`
                })
            } else {
                response.status(204).json({
                    data
                })
            }
        });
    }
}
module.exports = {getAllDrinks, createDrink, updateDrink, deleteDrink, getDrinkById}