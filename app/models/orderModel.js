const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const orderSchema = new Schema({
    _id:mongoose.Types.ObjectId,
    orderId:{
        type:String,
        unique:true,
    },
    kichCo:{
        type:String,
        required:true
    },
    duongKinh:{
        type:String,
        required:true
    },
    suon:{
        type:String,
        required:true
    },
    salad:{
        type:String,
        required:true
    },
    loaiPizza:{
        type:String,
        required:true
    },
    idVoucher:{
        type:mongoose.Types.ObjectId,
        ref:'voucher'
    },
    thanhTien:{
        type:Number,
        required:true
    },
    giamGia:{
        type:Number,
        required:false
    },
    idLoaiNuocUong:{
        type:mongoose.Types.ObjectId,
        ref:'drink'
    },
    soLuongNuoc:{
        type:Number,
        required:true
    },
    hoTen:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    soDienThoai:{
        type:Number,
        required:true
    },
    diaChi:{
        type:String,
        required:true
    },
    loiNhan:{
        type:String,
        required:false
    },
    trangThai:{
        type:String,
        required:true
    },
    ngayTao:{
        type:Date,
        default:Date.now()
    },
    ngayCapNhat:{
        type:Date,
        default:Date.now()
    }
})
module.exports = mongoose.model("order", orderSchema);