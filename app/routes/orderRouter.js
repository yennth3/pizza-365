//khai báo thư viện 
const express = require("express");

const orderRouter = express.Router();
const {createOrderOfUser, getAllOrderOfUser, getOrderById, updateOrderById, deleteOrderById, createNewOrder} = require("../controllers/orderController");

//get All orders
orderRouter.get("/users/:userid/orders", getAllOrderOfUser)

//create a orders
orderRouter.post("/users/:userid/orders", createOrderOfUser)

//get order by id
orderRouter.get("/orders/:orderId", getOrderById)

//update orders
orderRouter.put("/orders/:orderId", updateOrderById)

//delete orders
orderRouter.delete("/users/:userid/orders/:orderId", deleteOrderById)

//create new order
orderRouter.post("/devcamp-pizza365/orders", createNewOrder)

module.exports = orderRouter;