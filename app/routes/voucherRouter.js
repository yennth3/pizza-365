//khai báo thư viện 
const express = require("express");

const voucherRouter = express.Router();
const {getAllVouchers, createVoucher, updateVoucher, deleteVoucher, getVoucherById, getVoucherByVoucherCode}=require('../controllers/voucherController');


//get All vouchers
voucherRouter.get("/vouchers", getAllVouchers);

//get voucher by id
voucherRouter.get("/vouchers/:voucherid", getVoucherById);

//create a voucher
voucherRouter.post("/vouchers",createVoucher);

//update vouchers
voucherRouter.put("/vouchers/:voucherid", updateVoucher);

//delete vouchers
voucherRouter.delete("/vouchers/:voucherid",deleteVoucher);
//get voucher
voucherRouter.get("/devcamp-pizza365/voucher_detail/:voucherId",getVoucherByVoucherCode);

module.exports = voucherRouter;