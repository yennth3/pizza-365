//khai báo thư viện 
const express = require("express");

const drinkRouter = express.Router();

const {getAllDrinks, createDrink, updateDrink, deleteDrink, getDrinkById}=require('../controllers/drinkController');
//get All Drinks
drinkRouter.get("/drinks",getAllDrinks)
//getDrinkById
drinkRouter.get("/drinks/:drinkid",getDrinkById)
//create a drink
drinkRouter.post("/drinks", createDrink)

//update drink
drinkRouter.put("/drinks/:drinkid",updateDrink)

//delete drink
drinkRouter.delete("/drinks/:drinkid", deleteDrink)

//get drink
drinkRouter.get("/devcamp-pizza365/drinks",getAllDrinks)

module.exports = drinkRouter;