//khai báo thư viện 
const express = require("express");

const userRouter = express.Router();
const {createUser, getAllUsers, getUserById, updateUserById, deleteUserById, getAllLimitUsers, getAllSkipUsers} = require('../controllers/userController');
//get All users
userRouter.get("/users", getAllUsers)
//get All users limit
userRouter.get("/limit-users", getAllLimitUsers)
//get All users skip
userRouter.get("/skip-users", getAllSkipUsers)
//create a user
userRouter.post("/users", createUser);

//get user by id
userRouter.get("/users/:userId", getUserById);

//update users
userRouter.put("/users/:userid", updateUserById)

//delete user
userRouter.delete("/users/:userid", deleteUserById)

module.exports = userRouter;