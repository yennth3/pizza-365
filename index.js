const express = require('express');
const path = require('path');
const mongoose = require('mongoose');

const app = express();
const port = 8000;
app.use(express.static("views"));
//sử dụng body json
app.use(express.json());
//sử dụng body unicode
app.use(express.urlencoded({
    urlencoded:true
}))
//Khai báo các router sử dụng moddlewares
const drinkRouter = require('./app/routes/drinkRouter');
const orderRouter = require('./app/routes/orderRouter');
const userRouter = require('./app/routes/userRouter');
const voucherRouter = require('./app/routes/voucherRouter');
//khai váo các model
const drinkModel = require("./app/models/drinkModel");
const voucherModel = require("./app/models/voucherModel");
const orderModel = require("./app/models/orderModel");
const userModel = require("./app/models/userModel");
//sử dụng rourter
app.use('/', drinkRouter);
app.use('/', voucherRouter);
app.use('/', orderRouter);
app.use('/', userRouter);
//kết nối csdl mongodb
mongoose.connect('mongodb://localhost:27017/CRUD_Pizza365',(error)=>{
    if(error){
        throw error;
    }
    console.log("successfully connected!")
})
app.get("/", (request, response)=>{
    response.sendFile(path.join(__dirname+'/views/Pizza 365 v1.9.html'));
})


app.listen(port, () =>{
    console.log(`App listening on port ${port}`)
})